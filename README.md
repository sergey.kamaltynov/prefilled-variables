## Pre-filled variables

In 13.7, we’re introduced a feature that enables “Run pipeline” form to generate pre-filled variables for your pipeline based on the variable definitions in your .gitlab-ci.yml file. The response to this feature from the GitLab community was enthusiastic.


[Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/30101)


